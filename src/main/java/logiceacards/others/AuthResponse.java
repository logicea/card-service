package logiceacards.others;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthResponse {
    private String accessToken;
    private String refreshToken;
    private String expiresIn;
    private String issuedAt;
    private String tokenType;
}
