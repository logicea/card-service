package logiceacards.others;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BankObject {
    public int id;
    public String uid;
    public String brand;
    public String equipment;
}
