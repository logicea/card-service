package logiceacards.others;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class CardWrapper {
    private Long id;
    private String name;
    private String description;
    private String color;

    private String status;
    private String createdBy;
    private Date createdOn;

    private int page;
    private int size;

    private String sortField;
    private String sortOrder;
}
