package logiceacards.repository;


import com.sun.istack.NotNull;
import logiceacards.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    @Query(countQuery = "select count(u) from User u where u.softDelete = false")
    Page<User> findAllBySoftDeleteFalse(Pageable pageable);

    Optional<User> findByPhoneNumber(String phoneNumber);

    Optional<User> findByEmailAddress(String email);
    Optional<User> findById(@NotNull Long id);
}
