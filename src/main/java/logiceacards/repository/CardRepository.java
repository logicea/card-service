package logiceacards.repository;

import logiceacards.model.Card;
import logiceacards.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface CardRepository extends JpaRepository<Card, Long> {

    Optional<Card> findByName(String name);

    Optional<Card> findByIdAndCreatedBy(Long id, String username);

    @Query(countQuery = "select count(u) from Card u where u.softDelete = false and u.createdBy=:userName")
    Page<Card> findAllByCreatedByAndSoftDeleteFalse(Pageable pageable, String userName);

    @Query(countQuery = "select count(u) from Card u where u.softDelete = false")
    Page<Card> findAllBySoftDeleteFalse(Pageable pageable);

    Page<Card> findByColorOrNameOrStatusContainingIgnoreCaseAndCreatedBy(String color, String name, String status, String username, Pageable pageable);

    @Query("SELECT c FROM Card c WHERE " +
            "LOWER(c.color) LIKE %:searchTerm% OR " +
            "LOWER(c.name) LIKE %:searchTerm% OR " +
            "LOWER(c.status) LIKE %:searchTerm% OR " +
            "LOWER(c.createdOn) LIKE %:searchTerm%")
    List<Card> findByFieldsContainingIgnoreCase(String searchTerm);
}
