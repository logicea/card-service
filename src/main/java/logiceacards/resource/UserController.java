package logiceacards.resource;


import logiceacards.others.PaginationWrapper;
import logiceacards.others.UserWrapper;
import logiceacards.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {
    private final UserService userService;
    @PostMapping("/create")
    public Mono<ResponseEntity<?>> createUser(@RequestBody UserWrapper wrapper) {
        return userService.createUser(wrapper)
                .map(res -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(res));
    }


}
