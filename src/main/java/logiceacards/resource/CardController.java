package logiceacards.resource;

import logiceacards.others.CardWrapper;
import logiceacards.others.PaginationWrapper;
import logiceacards.others.UserWrapper;
import logiceacards.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/card")
public class CardController {
    private final CardService cardService;

    @PostMapping("/create")
    public Mono<ResponseEntity<?>> createCard(@RequestBody CardWrapper wrapper, @ModelAttribute String createdBy) {
        return cardService.createCard(wrapper, createdBy)
                .map(res -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(res));
    }

    @PostMapping("/get-my-cards")
    public Mono<ResponseEntity<?>> getMyCards(@RequestBody PaginationWrapper wrapper, @ModelAttribute String username) {
        return cardService.getMyCards(wrapper, username)
                .map(res -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(res));
    }

    @PostMapping("/get-card-by-id")
    public Mono<ResponseEntity<?>> getCardById(@RequestParam(value = "cardId") Long id, @ModelAttribute String username) {
        return cardService.getCardById(id, username)
                .map(res -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(res));
    }
    @PreAuthorize("hasRole('ROLE_ADMIN')") // Add the PreAuthorize annotation here
    @PostMapping("/get-all-cards")
    public Mono<ResponseEntity<?>> getAllCards(@RequestBody PaginationWrapper wrapper, @ModelAttribute String username) {
        return cardService.getAllCards(wrapper, username)
                .map(res -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(res));
    }

    @PutMapping("/update")
    public Mono<ResponseEntity<?>> updateCard(@RequestParam(value = "cardId") Long id, @RequestBody CardWrapper wrapper, @ModelAttribute String modifiedBy) {
        return cardService.updateCard(id, wrapper, modifiedBy)
                .map(res -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(res));
    }

    @DeleteMapping("/delete")
    public Mono<ResponseEntity<?>> deleteCard(@RequestParam(value = "cardId") Long id, @ModelAttribute String username) {
        return cardService.deleteCard(id, username)
                .map(res -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(res));
    }


    @PostMapping("/search")
    public Mono<ResponseEntity<?>> searchCard(@RequestBody CardWrapper wrapper, @ModelAttribute String username) {
        return cardService.searchCard(wrapper,username)
                .map(res -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(res));
    }

}
