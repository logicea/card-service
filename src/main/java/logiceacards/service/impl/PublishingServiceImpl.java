package logiceacards.service.impl;

import com.google.gson.Gson;
import logiceacards.service.PublishingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Map;

import static logiceacards.constants.KafkaTopics.*;
@Service
@Slf4j
@RequiredArgsConstructor
public class PublishingServiceImpl implements PublishingService {
    private final StreamBridge streamBridge;
    private final Gson gson;

    @Override
    public void createUserCredentials(String username, String userType, String portalUrl) {
        Mono.fromRunnable(() -> {
            Map<String, String> eventPayload = Map.of("username", username, "userType", userType, "portalUrl", portalUrl);
            streamBridge.send(CREATE_CUSTOMER_CREDENTIALS, gson.toJson(eventPayload));
        }).publishOn(Schedulers.boundedElastic()).subscribeOn(Schedulers.boundedElastic()).subscribe();
    }
}
