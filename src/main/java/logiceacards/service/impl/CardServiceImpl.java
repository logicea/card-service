package logiceacards.service.impl;

import io.micrometer.core.lang.Nullable;
import logiceacards.model.Card;
import logiceacards.model.User;
import logiceacards.others.CardWrapper;
import logiceacards.others.PaginationWrapper;
import logiceacards.others.UniversalResponse;
import logiceacards.repository.CardRepository;
import logiceacards.service.CardService;
import logiceacards.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class CardServiceImpl implements CardService {

    private final CardRepository cardRepository;
    @Lazy
    @Autowired
    private UserService userService;

    @Lazy
    @Autowired
    private CardService cardService;

    @Override
    public Optional<Card> findByName(String name) {
        return cardRepository.findByName(name);
    }

    @Override
    public Mono<UniversalResponse> createCard(CardWrapper wrapper, String createdBy) {
        return Mono.fromCallable(() -> {
            //validate the user creating the card
            UniversalResponse User_not_found = validateLoggedUser(createdBy);
            if (User_not_found != null) return User_not_found;

            //check for duplicates
            Optional<Card> cardOptional = cardService.findByName(wrapper.getName());
            if (cardOptional.isPresent()) return new UniversalResponse("01", "Duplicate found");

            // instantiate a card object or builder from the card entity
            Card cardBody = Card.builder()
                    .name(wrapper.getName())
                    .color(wrapper.getColor())
                    .description(wrapper.getDescription())
                    .createdBy(createdBy)
                    .status("To Do")
                    .lastModifiedBy(createdBy)
                    .build();

            // save the card
            createUpdateCard(cardBody);
            return new UniversalResponse("00", "Card created successfully !");
        }).publishOn(Schedulers.boundedElastic());
    }


    @Override
    public Card createUpdateCard(Card card) {
        return cardRepository.save(card);
    }

    @Override
    public Mono<UniversalResponse> getMyCards(PaginationWrapper wrapper, String username) {
        return Mono.fromCallable(() -> {
            //validate user
            UniversalResponse User_not_found = validateLoggedUser(username);
            if (User_not_found != null) return User_not_found;

            PageRequest pageRequest = PageRequest.of(wrapper.getPage(), wrapper.getSize());
            Page<Card> cardPage = cardRepository.findAllByCreatedByAndSoftDeleteFalse(pageRequest, username);

            return getCardsResponse(cardPage);

        }).publishOn(Schedulers.boundedElastic());
    }

    @Override
    public Mono<UniversalResponse> getAllCards(PaginationWrapper wrapper, String username) {
        return Mono.fromCallable(() -> {
            //validate user
            UniversalResponse User_not_found = validateLoggedUser(username);
            if (User_not_found != null) return User_not_found;

            PageRequest pageRequest = PageRequest.of(wrapper.getPage(), wrapper.getSize());
            Page<Card> cardPage = cardRepository.findAllBySoftDeleteFalse(pageRequest);

            return getCardsResponse(cardPage);
        }).publishOn(Schedulers.boundedElastic());
    }

    @Override
    public Mono<UniversalResponse> getCardById(Long id, String username) {
        return Mono.fromCallable(() -> {
            UniversalResponse User_not_found = validateLoggedUser(username);
            if (User_not_found != null) return User_not_found;

            //check if the user to be updated is present
            Optional<Card> optionalCard = cardService.findByIdAndCreatedBy(id, username);
            if (optionalCard.isEmpty()) return new UniversalResponse("01", "Card not found");
            Card card = optionalCard.get();

            return new UniversalResponse("00", "Card", card);

        }).publishOn(Schedulers.boundedElastic());
    }

    @Override
    public Optional<Card> findByIdAndCreatedBy(Long id, String username) {
        return cardRepository.findByIdAndCreatedBy(id, username);
    }

    @Override
    public Mono<UniversalResponse> updateCard(Long id, CardWrapper wrapper, String modifiedBy) {
        return Mono.fromCallable(() -> {
            //validate the modifier first
            UniversalResponse User_not_found = validateLoggedUser(modifiedBy);
            if (User_not_found != null) return User_not_found;

            //check if the user to be updated is present
            Optional<Card> cardOptional = cardService.findByIdAndCreatedBy(id, modifiedBy);
            if (cardOptional.isEmpty()) return new UniversalResponse("01", "Card not found");

            Card card = cardOptional.get();

            //update our user
            card.setName(wrapper.getName());
            card.setColor(wrapper.getColor());
            card.setDescription(wrapper.getDescription());
            card.setLastModifiedBy(modifiedBy);

            //update user
            cardService.createUpdateCard(card);
            return new UniversalResponse("00", "Card updated successfully !");

        }).publishOn(Schedulers.boundedElastic());
    }

    @Override
    public Mono<UniversalResponse> deleteCard(Long id, String username) {
        return Mono.fromCallable(() -> {
            UniversalResponse User_not_found = validateLoggedUser(username);
            if (User_not_found != null) return User_not_found;

            //check if the user to be updated is present
            Optional<Card> optionalCard = cardService.findByIdAndCreatedBy(id, username);
            if (optionalCard.isEmpty()) return new UniversalResponse("01", "User not found");
            Card card = optionalCard.get();

            cardRepository.delete(card);
            return new UniversalResponse("00", "Card deleted successfully !");
        }).publishOn(Schedulers.boundedElastic());
    }

    @Override
    public Mono<UniversalResponse> searchCard(CardWrapper wrapper, String username) {
        return Mono.fromCallable(() -> {
            UniversalResponse User_not_found = validateLoggedUser(username);
            if (User_not_found != null) return User_not_found;

            Sort sort = Sort.by(Sort.Direction.fromString(wrapper.getSortOrder()), wrapper.getSortField());

            PageRequest pageRequest = PageRequest.of(wrapper.getPage(), wrapper.getSize(),sort);
            Page<Card> cardPage = cardRepository.findByColorOrNameOrStatusContainingIgnoreCaseAndCreatedBy(
                    wrapper.getColor(),
                    wrapper.getName(), wrapper.getStatus(), username,
                    pageRequest);

            List<CardWrapper> collect = cardPage.getContent()
                    .stream()
                    .map(this::mapToCardWrapper)
                    .collect(Collectors.toList());


            return new UniversalResponse("00", "Cards", collect);
        }).publishOn(Schedulers.boundedElastic());
    }

    private UniversalResponse getCardsResponse(Page<Card> cardPage) {
        List<CardWrapper> collect = cardPage.getContent()
                .stream()
                .map(this::mapToCardWrapper)
                .collect(Collectors.toList());

        Map<String, Object> metadata = Map.of("numofrecords", cardPage.getTotalElements());
        return UniversalResponse.builder()
                .status("00")
                .message("All Cards !")
                .data(collect)
                .metadata(metadata)
                .timestamp(new Date())
                .build();
    }

    private CardWrapper mapToCardWrapper(Card card) {
        return CardWrapper.builder()
                .id(card.getId())
                .name(card.getName())
                .color(card.getColor())
                .description(card.getDescription())
                .createdBy(card.getCreatedBy())
                .createdOn(card.getCreatedOn())
                .build();
    }

    @Nullable
    private UniversalResponse validateLoggedUser(String createdBy) {
        //validate the modifier first
        Optional<User> createBy = userService.findByEmailAddress(createdBy);
        if (createBy.isEmpty()) return new UniversalResponse("01", "User not found");
        return null;
    }
}
