package logiceacards.service.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import logiceacards.others.AuthResponse;
import logiceacards.others.UniversalResponse;
import logiceacards.service.ExternalIntegration;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import javax.annotation.PostConstruct;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExternalIntegrationImpl implements ExternalIntegration {
    private final Gson gson;
    @Value("${jenga.jenga-api-key}")
    private String apiKey;
    @Value("${jenga.merchantCode}")
    private String merchantCode;

    @Value("${jenga.consumerSecret}")
    private String consumerSecret;

    @Value("${jenga.host-uri}")
    private String hostUri;

    private WebClient webClient;

    @PostConstruct
    public void init() {
        webClient = WebClient.builder().baseUrl(hostUri).build();
    }


}
