package logiceacards.service.impl;

import com.google.gson.Gson;
import io.micrometer.core.lang.Nullable;
import logiceacards.constants.UserType;
import logiceacards.model.User;
import logiceacards.others.AuthResponse;
import logiceacards.others.PaginationWrapper;
import logiceacards.others.UniversalResponse;
import logiceacards.others.UserWrapper;
import logiceacards.repository.UserRepository;
import logiceacards.security.user.UserEntity;
import logiceacards.security.user.UserEntityRepository;

import logiceacards.service.ExternalIntegration;
import logiceacards.service.PublishingService;
import logiceacards.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service

//A Lombok annotation in Java that automatically generates a constructor for a class with final fields as arguments
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserEntityRepository userEntityRepository;
    private final Gson gson;

    @Value("${external.portal}")
    private String portalUrl;

    private final PublishingService publishingService;


    @Lazy
    @Autowired
    private UserService userService;
    private final UserRepository userRepository;

    @Override
    public Mono<UserEntity> findByUsername(String username) {
        return Mono.fromCallable(() -> findUserByUsername(username).orElse(null))
                .publishOn(Schedulers.boundedElastic());
    }

    private Optional<UserEntity> findUserByUsername(String username) {
        return userEntityRepository.findByUsername(username);
    }

    @Override
    public Optional<User> findByPhoneNumber(String phoneNumber) {
        return userRepository.findByPhoneNumber(phoneNumber);
    }

    @Override
    public Optional<User> findByEmailAddress(String username) {
        return userRepository.findByEmailAddress(username);
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User createUpdateUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public UserEntity saveUser(UserEntity user) {
        return userEntityRepository.save(user);
    }

    @Override
    public Mono<UniversalResponse> createUser(UserWrapper wrapper) {
        return Mono.fromCallable(() -> {
            //check for duplicates
            Optional<User> optionalUser = userService.findByEmailAddress(wrapper.getEmailAddress());
            if (optionalUser.isPresent()) return new UniversalResponse("01", "Duplicate found");

            // instantiate a user object or builder from the user entity
            User userBody = User.builder()
                    .age(wrapper.getAge())
                    .active(true)
                    .firstName(wrapper.getFirstName())
                    .lastName(wrapper.getLastName())
                    .nationalId(wrapper.getNationalId())
                    .phoneNumber(wrapper.getPhoneNumber())
                    .emailAddress(wrapper.getEmailAddress())
                    .createdBy(wrapper.getEmailAddress())
                    .build();
            //save user
            createUpdateUser(userBody);

            // publish an event to create credentials for the user on auth service;
            publishingService.createUserCredentials(wrapper.getEmailAddress(), UserType.CUSTOMER.name(), portalUrl);

            return new UniversalResponse("00", "User created successfully !");
        }).publishOn(Schedulers.boundedElastic());
    }

    @Nullable
    private UniversalResponse validateLoggedUser(String createdBy) {
        //validate the modifier first
        Optional<User> createBy = userService.findByPhoneNumber(createdBy);
        if (createBy.isEmpty()) return new UniversalResponse("01", "User not found");
        return null;
    }

    private UserWrapper mapToUserWrapper(User user) {
        return UserWrapper.builder()
                .id(user.getId())
                .nationalId(user.getNationalId())
                .phoneNumber(user.getPhoneNumber())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .active(user.isActive())
                .age(user.getAge())
                .build();
    }

}
