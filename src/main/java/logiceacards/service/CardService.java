package logiceacards.service;

import logiceacards.model.Card;
import logiceacards.others.CardWrapper;
import logiceacards.others.PaginationWrapper;
import logiceacards.others.UniversalResponse;
import reactor.core.publisher.Mono;

import java.util.Optional;

public interface CardService {
    Mono<UniversalResponse> createCard(CardWrapper wrapper, String createdBy);

    Optional<Card> findByName(String name);

    Card createUpdateCard(Card card);

    Mono<UniversalResponse> getMyCards(PaginationWrapper wrapper, String username);

    Mono<UniversalResponse> getAllCards(PaginationWrapper wrapper, String username);

    Mono<UniversalResponse> getCardById(Long id, String username);

    Optional<Card> findByIdAndCreatedBy(Long id, String username);

    Mono<UniversalResponse> updateCard(Long id, CardWrapper wrapper, String modifiedBy);

    Mono<UniversalResponse> deleteCard(Long id, String username);

    Mono<UniversalResponse> searchCard(CardWrapper wrapper, String username);
}
