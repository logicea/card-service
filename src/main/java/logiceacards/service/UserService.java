package logiceacards.service;

import logiceacards.model.User;
import logiceacards.others.PaginationWrapper;
import logiceacards.others.UniversalResponse;
import logiceacards.others.UserWrapper;
import logiceacards.security.user.UserEntity;
import reactor.core.publisher.Mono;

import java.util.Optional;

public interface UserService {
    Mono<UserEntity> findByUsername(String username);

    Optional<User> findByPhoneNumber(String phoneNumber);


    Optional<User> findByEmailAddress(String username);

    Optional<User> findById(Long id);

    User createUpdateUser(User user);

    UserEntity saveUser(UserEntity user);

    Mono<UniversalResponse> createUser(UserWrapper wrapper);

}
