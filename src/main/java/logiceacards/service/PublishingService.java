package logiceacards.service;

public interface PublishingService {
    void createUserCredentials(String username, String userType, String portalUrl);

}
