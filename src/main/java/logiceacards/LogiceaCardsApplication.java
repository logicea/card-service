package logiceacards;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;

import io.netty.handler.ssl.SslContext;



import javax.net.ssl.SSLException;

@SpringBootApplication
public class LogiceaCardsApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogiceaCardsApplication.class, args);
	}
	@Bean
	public WebClient.Builder webClient() throws SSLException {
		SslContext sslContext = SslContextBuilder
				.forClient()
				.trustManager(InsecureTrustManagerFactory.INSTANCE)
				.build();
		HttpClient httpClient = HttpClient.create().secure(t -> t.sslContext(sslContext));
		return WebClient.builder().clientConnector(new ReactorClientHttpConnector(httpClient));
	}

}
