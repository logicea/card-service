package logiceacards.model;

import logiceacards.model.audit.Auditable;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "cards")
public class Card extends Auditable {
    @Column(unique = true)
    private String name;
    private String description;
    private String color;
    private String status = "To Do"; // Default status is "To Do"
    @CreatedBy
    private String createdBy;
    @LastModifiedBy
    private String lastModifiedBy;
}
