package logiceacards.model;


import logiceacards.model.audit.Auditable;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class User extends Auditable {
    private String firstName;
    private String nationalId;
    @Column(unique = true)
    private String phoneNumber;
    @Column(unique = true)
    private String emailAddress;
    private String lastName;
    private int age;
    private boolean active = true;

    @CreatedBy
    private String createdBy;
    @LastModifiedBy
    private String lastModifiedBy;
}
