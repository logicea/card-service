package logiceacards.constants;

public enum UserType {
    CUSTOMER, ADMIN
}
