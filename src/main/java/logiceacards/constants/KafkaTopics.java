package logiceacards.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class KafkaTopics {
    public static final String SMS_TOPIC = "mb-sms-topic";
    public static final String RESET_PASSWORD_TOPIC = "update-password-topic";
    public static final String FORGOT_PASSWORD_TOPIC = "forgot-password-topic";
    public static final String CREATE_CUSTOMER_CREDENTIALS = "create-credentials-topic";
    public static final String CREATE_BC_ACCOUNT = "create-blockchain-account-topic";
    public static final String ENROLL_BC_ACCOUNT = "enroll-blockchain-account-topic";
    public static final String UPDATE_SQN_STATUS = "update-sqn-status-topic";
    public static final String UPDATE_AUTH_FACE = "auth-face-enroll-topic";
    public static final String UPDATE_AUTH_VOICE = "auth-voice-enroll-topic";
    public static final String RESET_PASSWORD_PORTAL = "reset-pin-topic";
    public static final String STAGE_ACCOUNT_OPENING_REQUEST = "stage-account-opening-topic";
}